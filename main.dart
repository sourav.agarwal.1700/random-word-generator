
import 'dart:math';

import 'package:fapps/database_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:flutter/painting.dart';
import 'dart:ui' as ui;


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //final wordPair = WordPair.random();
    return const MaterialApp(
      title: 'Startup Name Generator',
      home: RandomWords(),
    );
  }
}


class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  _RandomWordsState createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  bool alpha = true;
  final _saved = <WordPair>{};
  final _biggerFont = TextStyle(
      fontSize: 18.0,
      foreground: Paint()
      ..shader = ui.Gradient.linear(
        const Offset(0, 100),
        const Offset(200, 0),
        <Color>[
          Colors.red,
          Colors.green,
        ],
      )
  );



  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
          builder: (context) {
            final tiles = _saved.map(
                (pair) {
                  return ListTile(
                    title: Text(
                      pair.asPascalCase ,
                      style: _biggerFont,
                    ),
                  );
                },
            );
            final divided = tiles.isNotEmpty ?
                ListTile.divideTiles(tiles: tiles, context: context,).toList()
                : <Widget>[] ;
            return Scaffold(
              appBar: AppBar(
                title: const Text('Saved Suggestions'),
              ),
              body: ListView(children: divided),
            );
          }
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Startup Name Generator'),
        backgroundColor: Colors.orangeAccent ,
        actions: [
          IconButton(
              onPressed: _pushSaved,
              icon: const Icon(Icons.list),
              tooltip: 'Saved Suggestions',
          ),
        ],
        //foregroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Expanded(child: _buildSuggestions()),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: const Text('Sort List'),
            //content: const Text('AlertDialog description'),
            actions: <Widget>[

              TextButton(
                onPressed: () { //=> Navigator.pop(context, 'Asc'),
                  alpha = false;
                  if (kDebugMode) {
                    print('Pressed $alpha');
                  }
                },

                  child: const Text('A-Z'),
              ),
              TextButton(
                onPressed: () { //=> Navigator.pop(context, 'Dsc'),
                  alpha = true;
                  if (kDebugMode) {
                    print("Pressed $alpha");
                  }
                },
                child: const Text('Z-A'),
              ),
            ],
          ),
        ),/*() async {
          List<Map<String,dynamic>> queryRows = await DatabaseHelper.instance.queryAll();
          //if (kDebugMode) {
            print(queryRows);
          //}
          /*showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Text('PopUp'),
              content: queryRows,
            )
          ),*/
        },*/
        child: const Icon(Icons.sort ),
        //Icon: const Icon(Icons.alarm),
      ),
    );
  }

  Widget _buildRow (WordPair pair) {
    final alreadySaved = _saved.contains(pair);
    String str = pair.asPascalCase ;
    int len = str.length;
    int l = len~/2;
    int i = ((l%2==0)?(l):(l+1));
    String str1 = str.substring(0,i);
    String str2 = str.substring(i,len);

    return ListTile(
      title: RichText(
        text: TextSpan(
          text: str1,
          style: const TextStyle(color: Colors.red),
          children: <TextSpan>[
            TextSpan(text: str2, style: const TextStyle(color: Colors.green)),
          ],
        ),
      ),
      /*title: Text(
        pair.asPascalCase,
        style: _biggerFont,

      ),*/
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved? Colors.red : null,
        semanticLabel: alreadySaved? 'Remove from Saved' : 'Save',
      ),
      onTap: () async {
        setState(() {
          if(alreadySaved) {
            _saved.remove(pair);
            //enter delete statements here for db
            //int rowDel = await DatabaseHelper.instance.delete(id);

          } else {
            _saved.add(pair);
            //enter insert statements here for db

           Future<int> a = DatabaseHelper.instance.insert({
              DatabaseHelper.columnName : pair
            }) ;

            if (kDebugMode) {
              print(' the inserted id is $a');
            }
          }
        });
      },
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
      padding: const EdgeInsets.all(16.0),
      itemCount: 100,
      itemBuilder: (context,i) {
        if (i.isOdd) return const Divider();
        final index = i; //i~/2;

        //if(index>=_suggestions.length) {
        _suggestions.addAll(generateWordPairs(random: Random()).take(15));
        //}

        /*if (alpha == false) {
          final sortedItems = _suggestions
            ..sort((item1, item2) =>
                item1.toString().compareTo(item2.toString()));
          final item = sortedItems[index];
          return _buildRow(item);
        }*/
        //else {
          //return _buildRow(item);
          return _buildRow(_suggestions[index]);
        //}
      }
    );


  }
}
